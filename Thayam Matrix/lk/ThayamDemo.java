package finalthayam.lk;

import java.util.Scanner;

public class ThayamDemo {

	int[][] matrixone, matrixtwo;
	int rows, cols;
	Scanner scanner;

	public void mathMatrixUserInput() {

		scanner = new Scanner(System.in);
		System.out.println("enter the number of rows :");
		rows = scanner.nextInt();
		System.out.println("enter the number of coloumns :");
		cols = scanner.nextInt();
	}

	public void matrixFindRowsCols() {

		matrixone = new int[rows][cols];
		matrixtwo = new int[rows][cols];

		System.out.println("enter   " + rows * cols + " digits for matrix one : ");
		for (

				int i = 0; i < rows; i++) {
			for (int j = 0; j < rows; j++)

				matrixone[i][j] = scanner.nextInt();
		}

		System.out.println("enter   " + rows * cols + " digits for matrix two : ");
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < rows; j++)
				matrixtwo[i][j] = scanner.nextInt();
		}
	}

	public void displayMatrixOne() {
		System.out.println("matrixone  :");
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < rows; j++)
				System.out.print("\t" + matrixone[i][j]);
			System.out.print("\n");
		}
	}

	public void displayMatrixTwo() {
		System.out.println("matrixtwo  :");
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < rows; j++)
				System.out.print("\t" + matrixtwo[i][j]);
			System.out.print("\n");
		}
	}

	public void displayFinalAnswer() {

		System.out.println("addition of both matrix ");

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < rows; j++)
				System.out.print("\t" + matrixone[i][j] + matrixone[i][j]);
			System.out.println("\n");
		}

	}
}
