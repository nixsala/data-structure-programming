package sortasc.lk;

public class AscendingArrayDemo {
	public static void main(String[] args) {
		int num[] = { 100, 40, 15, 20, 2 };
		AscendingArray ascendingArray = new AscendingArray();
		ascendingArray.displayAllArray(num);
		System.out.println("the minimum index number is:" + ascendingArray.findMinimum(num));
		System.out.println("the minimumindex element index  is:" + ascendingArray.findMinimumEle(num));
		ascendingArray.sortArray(num);
		ascendingArray.displayAfterSorting(num);

	}
}
