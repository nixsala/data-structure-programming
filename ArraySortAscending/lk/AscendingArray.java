package sortasc.lk;

public class AscendingArray {
	int index = 0;

	public int displayAllArray(int numArray[]) {
		System.out.println("Display all array element");
		for (int i = 0; i < numArray.length; i++) {
			System.out.println(numArray[i]);
		}
		return 0;
	}

	public int findMinimum(int numArray[]) {
		int min = numArray[0];
		for (int i = 0; i < numArray.length; i++) {
			if (min > numArray[i]) {
				min = numArray[i];

			}
		}
		return min;
	}

	public int findMinimumEle(int numArray[]) {
		int min = numArray[0];

		for (int i = 0; i < numArray.length; i++) {
			if (min > numArray[i]) {
				min = numArray[i];
				index = i;

			}
		}
		return index;
	}

	public int sortArray(int numArray[]) {

		for (int i = 0; i < numArray.length; i++) {
			for (int j = i + 1; j < numArray.length; j++) {
				if (numArray[i] > numArray[j]) {
					index = numArray[i];
					numArray[i] = numArray[j];
					numArray[j] = index;
				}
			}
		}
		return index;
	}

	public int displayAfterSorting(int numArray[]) {
		System.out.println("Display all array element after sorting :"+"Ascending order");
		for (int i = 0; i < numArray.length; i++) {
			System.out.print(numArray[i] + " ");
		}
		return 0;
	}
}